PRODUCT_PACKAGES += \
    PixelBuiltInPrintService \
    CaptivePortalLoginOverlay \
    CellBroadcastReceiverOverlay \
    CellBroadcastServiceOverlay \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GoogleWebViewOverlay \
    ManagedProvisioningPixelOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelConfigOverlayCommon \
    PixelConnectivityOverlay2022 \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizardOverlay2021 \
    PixelSetupWizard_rro \
    PixelTetheringOverlay2021 \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SettingsGoogleOverlay \
    SettingsGoogleOverlayPixel2022 \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    PixelTeleService \
    PixelTelecom \
    Pixelframework-res \
